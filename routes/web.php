<?php

use App\Http\Controllers\ShoppingCartController;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();


Route::get('/home', 'ProductController@product');
Route::get('/productView/{id}', 'ProductController@viewProduct');

Route::get('/addProduct', 'ProductController@addProduct');
Route::post('/addProductDB', 'ProductController@addProductDB');

Route::get('/editProduct/{id}', 'ProductController@editProduct');
Route::post('/editProductDB/{id}', 'ProductController@editProductDB');

Route::get('/addType', 'ProductController@addType');
Route::post('/addTypeDB', 'ProductController@addTypeDB');

Route::get('/deleteProductDB/{id}', 'ProductController@deleteProductDB');

Route::get('/editType', 'ProductController@editType');
Route::post('/updateType', 'ProductController@updateType');
Route::get('/deleteType/{id}', 'ProductController@deleteType');

Route::get('/shoppingcart', 'ShoppingCartController@index');
Route::post('/shoppingcart', 'ShoppingCartController@add');

Route::get('/shoppingcart/edit/{id}', 'ShoppingCartController@edit');
Route::put('/shoppingcart/editDB/{id}', 'ShoppingCartController@editDB');

Route::get('/shoppingcart/delete/{id}', 'ShoppingCartController@delete');

Route::get('/history', 'TransactionController@index');

Route::put('/shoppingcart/checkout', 'ShoppingCartController@checkout');

Route::get('/home/search', 'ProductController@search');


