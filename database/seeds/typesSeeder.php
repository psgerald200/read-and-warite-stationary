<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class typesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('types')->insert([
            ['name' => 'Alat Tulis'],
            ['name' => 'Alat Gambar'],
            ['name' => 'Bahan Kertas'],
            ['name' => 'Buku Tulis'],
            ['name' => 'Buku'],
            ['name' => 'Misc.'],
        ]);
    }
}
