<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class usersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    
    public function run()
    {
        DB::table('users')->insert([
            ['name' => 'member1', 'email' => 'member1@gmail.com', 'role' => 'Member', 'password' => Hash::make('member123')],
            ['name' => 'admin1', 'email' => 'admin1@gmail.com', 'role' => 'Admin', 'password' => Hash::make('admin123')]
        ]);
    }
}
