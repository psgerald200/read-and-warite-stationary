<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class productsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('products')->insert([
            [
                'name' => 'Pen',
                'stock' => 30,
                'price' => 5000,
                'description' => 'Pen mahal',
                'image' => 'pen.jpg',
                'type_id' => 1
            ],

            [
                'name' => 'Ballpoint',
                'stock' => 20,
                'price' => 25000,
                'description' => 'Ballpoint mahal',
                'image' => 'ballpoint.jpg',
                'type_id' => 1
            ],

            [
                'name' => 'Kamus',
                'stock' => 10,
                'price' => 75000,
                'description' => 'Kamus jepang',
                'image' => 'dictionary.jpg',
                'type_id' => 5
            ],

            [
                'name' => 'Notepad',
                'stock' => 10,
                'price' => 20000,
                'description' => 'Notepad keren',
                'image' => 'notepad.jpg',
                'type_id' => 4
            ]
        ]);
    }
}
