<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@200;600&display=swap" rel="stylesheet">
      
        <!-- Styles -->
        <link rel="stylesheet" href="{{asset('bootstrap.css?v=').time()}}" type="text/css">
        <link rel="stylesheet" href="{{asset('bootstrap.min.css?v=').time()}}" type="text/css">
        <link rel="stylesheet" href="{{asset('updateproducttype.css?v=').time()}}" type="text/css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    </head>

    <body>
    {{-- Navigation Bar--}}
        
        <nav class="navbar navbar-light fixed-top" style="background-color: #e3f2fd;">
          <div class="container-fluid">
            <div class="navbar-header">
              <a class="navbar-brand" href="/home">ReadAndWArite</a>
            </div>
            <form class="navbar-form navbar-left" action="/home/search" method="get">
              <div class="input-group">
                <input type="text" class="form-control" placeholder="Search" name="search" value="{{ old('search') }}">
                <div class="input-group-btn">
                  <button class="btn btn-default" type="submit"  id="pills-home-tab">
                    <i class="glyphicon glyphicon-search"></i>
                  </button>
                </div>
              </div>
            </form>
            <ul class="nav nav-pills"id="pills-tab" role="tablist">
              <li class="nav-item"><a class="nav-link dropdown-toggle" id="pills-home-tab" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false" aria-selected="true">Admin</a>
                <div class="dropdown-menu">
                  <a class="dropdown-item" href="#" id="pills-home-tab">Admin</a>
                  <div class="dropdown-divider"></div>
                  <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();" id="pills-home-tab">Log out</a>
                  <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                  @csrf
                  </form>
                </div>
              </li>
            </ul>
          </div>
        </nav>
       
        {{-- Content --}}
        <div class="container" id="admin-container">
            <div class="form-inline">
                <table class="table table-bordered">
                    <thead>
                      <tr>
                        <th scope="col" style="text-align: center">Number</th>
                        <th scope="col" style="text-align: center">Stationary Type Name</th>
                        <th scope="col" style="text-align: center">Action</th>
                      </tr>
                    </thead>
                    <tbody>
                      @foreach ($type as $t)
                      <tr>
                        <th scope="row" style="text-align: center">{{$t->id}}</th>
                        <td>{{$t->name}}</td>
                        <td class="input-table">
                          <form method="post" action="/updateType">
                          @csrf
                                <input type="hidden" name="id" value="{{ $t->id }}"> 
                                <input type="text" name ="name" placeholder = "Stationary Type" class="form-control" style="margin-left:-20%;"> 
                                <input type="submit" class="btn btn-primary" value="Update">
                                <a href="/deleteType/{{$t -> id}}" class="btn btn-danger">Hapus</a>
                          </form>
                        </td>
                      </tr>
                      @endforeach
                    </tbody>
                  </table>
                </div>
        </div>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    </body>
</html>
    
