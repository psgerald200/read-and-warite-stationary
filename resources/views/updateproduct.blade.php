<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@200;600&display=swap" rel="stylesheet">
      
        <!-- Styles -->
        <link rel="stylesheet" href="{{asset('bootstrap.css?v=').time()}}" type="text/css">
        <link rel="stylesheet" href="{{asset('bootstrap.min.css?v=').time()}}" type="text/css">
        <link rel="stylesheet" href="{{asset('updateproduct.css?v=').time()}}" type="text/css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    </head>

    <body>
    {{-- Navigation Bar--}}
        
        <nav class="navbar navbar-light fixed-top" style="background-color: #e3f2fd;">
          <div class="container-fluid">
            <div class="navbar-header">
              <a class="navbar-brand" href="/home">ReadAndWArite</a>
            </div>
            <form class="navbar-form navbar-left" action="/home/search" method="get">
              <div class="input-group">
                <input type="text" class="form-control" placeholder="Search" name="search" value="{{ old('search') }}">
                <div class="input-group-btn">
                  <button class="btn btn-default" type="submit"  id="pills-home-tab">
                    <i class="glyphicon glyphicon-search"></i>
                  </button>
                </div>
              </div>
            </form>
            <ul class="nav nav-pills"id="pills-tab" role="tablist">
              <li class="nav-item"><a class="nav-link dropdown-toggle" id="pills-home-tab" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false" aria-selected="true">Admin</a>
                <div class="dropdown-menu">
                  <a class="dropdown-item" href="#" id="pills-home-tab">Admin</a>
                  <div class="dropdown-divider"></div>
                  <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();" id="pills-home-tab">Log out</a>
                  <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                  @csrf
                  </form>
                </div>
              </li>
            </ul>
          </div>
        </nav>
       
        {{-- Content --}}
        <div class="container" id="admin-container">
            <form method="post" action="/editProductDB/{{$product->id}}">
            @csrf
                <div class="form-group">
                    <label>Name</label>
                    <input type="text" class="form-control"  placeholder="Name" name="name" value="{{old('name', $product->name)}}">
                    @if($errors->has('name'))
                      <div class="text-danger">
                          {{ $errors->first('name')}}
                      </div>
                    @endif
                </div>
                <div class="form-group">
                    <label>Description</label>
                    <input type="text" class="form-control" placeholder="Description" name="description" value="{{old('name', $product->description)}}">
                    @if($errors->has('description'))
                      <div class="text-danger">
                          {{ $errors->first('description')}}
                      </div>
                    @endif
                </div>
                <div class="form-group">
                    <label>Price</label>
                    <input type="number" class="form-control" placeholder="Price" name="price" value="{{old('name', $product->price)}}">
                    @if($errors->has('price'))
                      <div class="text-danger">
                          {{ $errors->first('price')}}
                      </div>
                    @endif
                </div>
                <div class="form-group">
                    <label>Stock</label>
                    <input type="number" class="form-control" placeholder="Stock" name="stock" value="{{old('name', $product->stock)}}">
                    @if($errors->has('stock'))
                      <div class="text-danger">
                          {{ $errors->first('stock')}}
                      </div>
                    @endif
                </div>  
                <button type="submit" class="btn btn-primary">Update Stationary Data</button>
              </form>
        </div>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    </body>
</html>
    
