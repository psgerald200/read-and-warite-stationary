<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@200;600&display=swap" rel="stylesheet">
      
        <!-- Styles -->
        <link rel="stylesheet" href="{{asset('bootstrap.css?v=').time()}}" type="text/css">
        <link rel="stylesheet" href="{{asset('bootstrap.min.css?v=').time()}}" type="text/css">
        <link rel="stylesheet" href="{{asset('welcome.css?v=').time()}}" type="text/css">

    </head>

    <body>
        {{-- Navigation Bar--}}
    
        <nav class="navbar navbar-expand-lg navbar-light bg-light" style="float: right;">
            <div class="collapse navbar-collapse " id="navbarTogglerDemo03">
                <button type="button" onclick="location.href='/login'" class="btn btn-outline-dark btn-nav" id="login">Login</button>
                <button type="button" onclick="location.href='/register'" class="btn btn-outline-dark btn-nav" id="register">Register</button>
            </div>
        </nav>
       
        {{-- Content --}}
        <div class="container" id="welcome-container">
            <h1>ReadAndWArite</h1>
            
            <form action="/home/search" method="get">
              <div class="active-cyan-4 mb-4" id="search-field">
                  <input type="text" style="padding: 3% 2%; font-size:24px" class="form-control" placeholder="Search" name="search" value="{{ old('search') }}">
              </div>
              <button type="submit" class="btn btn-primary search-btn">Search</button>
            </form>
            
        </div>

        <div class="card-deck" id="card-decks">
            <div class="card bg-transparent card-image shadow-lg p-3 mb-5 bg-white rounded">
              <div class="card-body text-center">
               <img src="ballpoint.jpg" width="300px" height="400px">
              </div>
            </div>
            <div class="card bg-transparent card-image shadow-lg p-3 mb-5 bg-white rounded">
              <div class="card-body text-center">
                <img src="notepad.jpg" width="300px" height="400px">
              </div>
            </div>
            <div class="card bg-transparent card-image shadow-lg p-3 mb-5 bg-white rounded">
              <div class="card-body text-center">
                <img src="dictionary.jpg" width="300px" height="400px">
              </div>
            </div>
            <div class="card bg-transparent card-image shadow-lg p-3 mb-5 bg-white rounded">
              <div class="card-body text-center ">
                <img src="pen.jpg" width="300px" height="400px">
              </div>
            </div>  
          </div>
          <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
          <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    </body>
</html>
