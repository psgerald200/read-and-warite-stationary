<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@200;600&display=swap" rel="stylesheet">
      
        <!-- Styles -->
        <link rel="stylesheet" href="{{asset('bootstrap.css?v=').time()}}" type="text/css">
        <link rel="stylesheet" href="{{asset('bootstrap.min.css?v=').time()}}" type="text/css">
        <link rel="stylesheet" href="{{asset('transactionhistory.css?v=').time()}}" type="text/css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    </head>

    <body>
    {{-- Navigation Bar--}}
        
        <nav class="navbar navbar-light fixed-top" style="background-color: #e3f2fd;">
          <div class="container-fluid">
            <div class="navbar-header">
              <a class="navbar-brand" href="/home">ReadAndWArite</a>
            </div>
            <form class="navbar-form navbar-left" action="/home/search" method="get">
              <div class="input-group">
                <input type="text" class="form-control" placeholder="Search" name="search" value="{{ old('search') }}">
                <div class="input-group-btn">
                  <button class="btn btn-default" type="submit"  id="pills-home-tab">
                    <i class="glyphicon glyphicon-search"></i>
                  </button>
                </div>
              </div>
            </form>
            <ul class="nav nav-pills"id="pills-tab" role="tablist">
              <li class="nav-item"><a class="nav-link dropdown-toggle" id="pills-home-tab" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false" aria-selected="true">Member</a>
                <div class="dropdown-menu">
                  <a class="dropdown-item" href="#" id="pills-home-tab">Member</a>
                  <div class="dropdown-divider"></div>
                  <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();" id="pills-home-tab">Log out</a>
                  <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                  @csrf
                  </form>
                </div>
              </li>
              <li class="nav-item">
                <a class="nav-link" id="pills-home-tabs" href="/shoppingcart" role="tab" aria-controls="pills-home" aria-selected="false">Cart</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" id="pills-home-tabs" href="/history" role="tab" aria-controls="pills-home" aria-selected="false">History</a>
              </li>
            </ul>
          </div>
        </nav>
       
        {{-- Content --}}
        <div class="container" id="admin-container">
            <h2 style="color: black">You don't have any transaction</h2>
            
            
            <div class="form-inline">
                <table class="table table-striped " id="table1">

                <?php $count = 0; ?>
                  @foreach ($transaction->unique('shoppingcart_id') as $tr)
                    <thead class="thead-dark">
                    <?php $total = 0; ?>
                      <tr>
                        <th scope="col" colspan="5" style="text-align: center">Date: {{$tr->created_at}}</th>
                        
                      </tr>
                    </thead>
                    <tbody >
                      @foreach($transaction as $t)
                      @if($t->shoppingcart_id == $count)
                      <tr>
                      <?php $subtotal = $t->product->price * $t->quantity; ?>
                      <?php $total += $subtotal; ?>
                        <td>{{$t->product->name}}</td>
                        <td>Rp. {{$t->product->price}}</td>
                        <td>Quantity: {{$t->quantity}}</td>
                        <td>Subtotal: Rp. {{$subtotal}}</td>
                      </tr>
                      @endif
                      
                    </tbody>
                    @endforeach
                    <?php $count += 1; ?>
                    <thead class="thead-light">
                    <tr>
                        <th  scope="col" colspan="5"style="text-align: center">TOTAL: Rp. {{$total}}</th>
                      </tr>
                    </thead>
                    
                    @endforeach
                  </table>
            </div>
        </div>

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    </body>
</html>
    
