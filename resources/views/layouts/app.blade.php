<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'ReadAndWArite') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@200;600&display=swap" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('bootstrap.css?v=').time()}}" type="text/css">
    <link rel="stylesheet" href="{{asset('bootstrap.min.css?v=').time()}}" type="text/css">
    <link rel="stylesheet" href="{{asset('login.css?v=').time()}}" type="text/css">
    <link rel="stylesheet" href="{{asset('register.css?v=').time()}}" type="text/css">
     <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-light fixed-top">
            <div class="container-fluid">
                <div class="navbar-header">
                  <a class="navbar-brand" href="">ReadAndWArite</a>
                </div>
                <form class="navbar-form navbar-left" action="/home/search" method="get">
              <div class="input-group">
                <input type="text" class="form-control" placeholder="Search" name="search" value="{{ old('search') }}">
                <div class="input-group-btn">
                  <button class="btn btn-default" type="submit"  id="pills-home-tab">
                    <i class="glyphicon glyphicon-search"></i>
                  </button>
                </div>
              </div>
            </form>
                <ul class="nav nav-pills"id="pills-tab" role="tablist">
                  <li class="nav-item">
                    <a href="/login" class="nav-link"  id="pills-home-tabs"  role="tab" aria-controls="pills-home" aria-selected="false">Login</a>
                  </li>
                  <li class="nav-item">
                    <a href="/register" class="nav-link" id="pills-home-tabs"  role="tab" aria-controls="pills-home" aria-selected="false">Register</a>
                  </li>
                </ul>
              </div>
        </nav>

        <main class="py-4">
            @yield('content')
        </main>
    </div>
</body>
</html>
