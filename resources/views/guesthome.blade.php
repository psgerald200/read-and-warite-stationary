<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@200;600&display=swap" rel="stylesheet">
      
        <!-- Styles -->
        <link rel="stylesheet" href="{{asset('bootstrap.css?v=').time()}}" type="text/css">
        <link rel="stylesheet" href="{{asset('bootstrap.min.css?v=').time()}}" type="text/css">
        <link rel="stylesheet" href="{{asset('memberhome.css?v=').time()}}" type="text/css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    </head>

    <body>
        {{-- Navigation Bar--}}
        
        <nav class="navbar navbar-light fixed-top" style="background-color: #e3f2fd;>
            <div class="container-fluid">
                <div class="navbar-header">
                  <a class="navbar-brand" href="/home">ReadAndWArite</a>
                </div>
                <form class="navbar-form navbar-left" action="/home/search" method="get">
              <div class="input-group">
                <input type="text" class="form-control" placeholder="Search" name="search" value="{{ old('search') }}">
                <div class="input-group-btn">
                  <button class="btn btn-default" type="submit"  id="pills-home-tab">
                    <i class="glyphicon glyphicon-search"></i>
                  </button>
                </div>
              </div>
            </form>
                <ul class="nav nav-pills"id="pills-tab" role="tablist">
                  <li class="nav-item">
                    <a href="/login" class="nav-link"  id="pills-home-tabs"  role="tab" aria-controls="pills-home" aria-selected="false">Login</a>
                  </li>
                  <li class="nav-item">
                    <a href="/register" class="nav-link" id="pills-home-tabs"  role="tab" aria-controls="pills-home" aria-selected="false">Register</a>
                  </li>
                </ul>
              </div>
        </nav>
       
        {{-- Content --}}
        <div class="container" id="member-container">
          @foreach($product as $p)
          <a href="">
          <div class="card-deck" id="card-decks">
            <div class="card bg-transparent card-image shadow-lg p-3 mb-5 bg-white rounded">
              <div class="card-body text-center">
               <h2>{{$p->name}}</h2>
               <img src="{{asset('/'.$p->image)}}" width="300px" height="400px">
               <h4>{{$p->description}}</h4>
              </div>
            </div>
          </div>
            </a>
            @endforeach
          </div>
          

          <nav aria-label="Page navigation example" style="text-align:center">
        <ul class="pagination justify-content-center pagination-lg">
           {{$product -> links()}}
        </ul>
    </nav>

          <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
          <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    </body>
</html>
