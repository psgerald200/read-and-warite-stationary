<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@200;600&display=swap" rel="stylesheet">
      
        <!-- Styles -->
        <link rel="stylesheet" href="{{asset('bootstrap.css?v=').time()}}" type="text/css">
        <link rel="stylesheet" href="{{asset('bootstrap.min.css?v=').time()}}" type="text/css">
        <link rel="stylesheet" href="{{asset('addstationarytype.css?v=').time()}}" type="text/css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    </head>

    <body>
        {{-- Navigation Bar--}}
        
        {{-- Navigation Bar--}}
        
        <nav class="navbar navbar-light fixed-top" style="background-color: #e3f2fd;">
          <div class="container-fluid">
            <div class="navbar-header">
              <a class="navbar-brand" href="/home">ReadAndWArite</a>
            </div>
            <form class="navbar-form navbar-left" action="/home/search" method="get">
              <div class="input-group">
                <input type="text" class="form-control" placeholder="Search" name="search" value="{{ old('search') }}">
                <div class="input-group-btn">
                  <button class="btn btn-default" type="submit"  id="pills-home-tab">
                    <i class="glyphicon glyphicon-search"></i>
                  </button>
                </div>
              </div>
            </form>
            <ul class="nav nav-pills"id="pills-tab" role="tablist">
              <li class="nav-item"><a class="nav-link dropdown-toggle" id="pills-home-tab" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false" aria-selected="true">Admin</a>
                <div class="dropdown-menu">
                  <a class="dropdown-item" href="#" id="pills-home-tab">Admin</a>
                  <div class="dropdown-divider"></div>
                  <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();" id="pills-home-tab">Log out</a>
                  <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                  @csrf
                  </form>
                </div>
              </li>
            </ul>
          </div>
        </nav>

        @if (Session::has('success'))
          <div class="alert alert-info">{{ Session::get('success') }}</div>
        @endif 

        {{-- Content --}}
        <div id="admin-container">
            <div class="form-inline">
                <table class="table table-striped table-dark" id="table1">
                    <thead>
                      <tr>
                        <th scope="col" style="text-align: center">Number</th>
                        <th scope="col" style="text-align: center">Stationary Type Name</th>
                      </tr>
                    </thead>
                    <tbody>
                      @foreach ($type as $t)
                      <tr>
                        <th scope="row" style="text-align: center">{{$t->id}}</th>
                        <td>{{$t->name}}</td>
                      </tr>
                      @endforeach
                    </tbody>
                  </table>

                <form method="post" action="addTypeDB"> 
                  @csrf
                    
                   
                    <div class="form-group gr1">
                        <label>Add New Stationary Type</label> 
                        <input type="text" name="name" class="form-control"  placeholder="Stationary Type">
                    </div>
                    @if($errors->has('name'))
                    <div class="text-danger">
                        {{ $errors->first('name')}}
                    </div>
                    @endif
                    
                    <br>
                    <br>
                    <div class="form-group gr1">
                        <input type="submit" class="btn btn-primary" style="margin-top: 10px;" value="Add New Stationary Type">
                    </div>
                </form>    
            </div>
        </div>

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    </body>
</html>
    
