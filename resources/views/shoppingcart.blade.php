<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@200;600&display=swap" rel="stylesheet">
      
        <!-- Styles -->
        <link rel="stylesheet" href="{{asset('bootstrap.css?v=').time()}}" type="text/css">
        <link rel="stylesheet" href="{{asset('bootstrap.min.css?v=').time()}}" type="text/css">
        <link rel="stylesheet" href="{{asset('shoppingcart.css?v=').time()}}" type="text/css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    </head>

    <body>
    {{-- Navigation Bar--}}
        
        <nav class="navbar navbar-light fixed-top" style="background-color: #e3f2fd;">
          <div class="container-fluid">
            <div class="navbar-header">
              <a class="navbar-brand" href="/home">ReadAndWArite</a>
            </div>
            <form class="navbar-form navbar-left" action="/home/search" method="get">
              <div class="input-group">
                <input type="text" class="form-control" placeholder="Search" name="search" value="{{ old('search') }}">
                <div class="input-group-btn">
                  <button class="btn btn-default" type="submit"  id="pills-home-tab">
                    <i class="glyphicon glyphicon-search"></i>
                  </button>
                </div>
              </div>
            </form>
            <ul class="nav nav-pills"id="pills-tab" role="tablist">
              <li class="nav-item"><a class="nav-link dropdown-toggle" id="pills-home-tab" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false" aria-selected="true">Member</a>
                <div class="dropdown-menu">
                  <a class="dropdown-item" href="#" id="pills-home-tab">Member</a>
                  <div class="dropdown-divider"></div>
                  <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();" id="pills-home-tab">Log out</a>
                  <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                  @csrf
                  </form>
                </div>
              </li>
              <li class="nav-item">
                <a class="nav-link" id="pills-home-tabs" href="/shoppingcart" role="tab" aria-controls="pills-home" aria-selected="false">Cart</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" id="pills-home-tabs" href="/history" role="tab" aria-controls="pills-home" aria-selected="false">History</a>
              </li>
            </ul>
          </div>
        </nav>
       
        {{-- Content --}}
        <div class="container" id="admin-container">
            @foreach ($shoppingcart as $sc)
            <h2 style="color: black; display:none;" class="desc">Do some transaction to see your product in cart</h2>
            @endforeach

            @foreach ($shoppingcart as $sc)
            <?php $total = $sc->product->price * $sc->quantity; ?>
            <div class="card">
           
                <h3>Stationary Name: {{$sc-> product -> name}}</h3>
                <br>
                <h4>- Stationary Price: {{$sc-> product -> price}}</h4>
                <h4>- Quantity: {{$sc-> quantity}}</h4>
                <br>
                <br>
                
                <h4>Total: Rp. {{$total}}</h4>
                <br>
                <div class="buttons">
                    <a href="/shoppingcart/edit/{{$sc->id}}" class="btn btn-primary" >Edit Item</a>
                    <a href="/shoppingcart/delete/{{$sc->id}}" class="btn btn-danger" >Delete Item</a>
                </div>
            </div>
            @endforeach
            
            

            <br>
            <form method="POST" action="/shoppingcart/checkout">
                @csrf
                {{ method_field('PUT') }}
              <input type="submit" class="btn btn-danger mt-5" style="float: left" value="Checkout">
            </form>
            
        </div>

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    </body>
</html>
    
