<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    public function Product(){
        return $this->belongsTo(Product::class);
    }
    

    protected $table = 'transactions';

    protected $fillable = ['user_id', 'shoppingcart_id', 'product_id', 'quantity'];
}
