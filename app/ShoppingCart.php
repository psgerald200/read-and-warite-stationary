<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ShoppingCart extends Model
{
    public function Product(){
        return $this->belongsTo(Product::class);
    }

    

    protected $table = 'shoppingcarts';

    protected $fillable = ['user_id', 'product_id', 'name', 'price', 'description', 'image', 'typename','quantity'];
}
