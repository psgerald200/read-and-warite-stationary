<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Type extends Model
{
    public function Product(){
        return $this->hasMany(Product::class);
    }

    public $table = 'types';

    
    protected $fillable = ['name'];
}
