<?php

namespace App\Http\Controllers;

use App\Product;
use App\Type;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class ProductController extends Controller
{
    public function product(){
        $product = Product::paginate(4);
        $user = auth()->user();

        if (Auth::guest())
        {
            return view ('guesthome', ['product' => $product]);
        }

        if ($user->role == 'Admin')
            {
                return view('adminhome', ['product' => $product]);
            }
        else if ($user->role == 'Member')
        {
            return view('memberhome', ['product' => $product]);
        }
        
    }

    public function viewProduct($id){
        $detail = Product::find($id);
        $user = auth()->user();

        if ($user->role == 'Admin')
            {
                return view('adminviewproduct', ['detail' => $detail]);
            }
        else if ($user->role == 'Member')
        {
            return view('memberviewproduct', ['detail' => $detail]);
        }
        
    }

    public function editProduct($id){
        $product = Product::find($id);
        $user = auth()->user();

        if ($user->role =='Admin'){
            return view('updateproduct', ['product' => $product]);
        }
    }

    public function editProductDB($id, Request $request){
        $product = Product::find($id);
        $user = auth()->user();

        if ($user->role =='Admin'){
            $this->validate($request,[
                'name' => ['required', 'string', 'min:5', 'unique:products'],
                'description' => ['required', 'string', 'min:10'],
                'stock' => ['required', 'integer', 'min:1'],
                'price' => ['required', 'integer', 'min:5000'],
            ]);

            $product->name = $request->name;
            $product->description = $request->description;
            $product->stock = $request->stock;
            $product->price = $request->price;
            $product->save();

            return redirect('/home');
        }
    }

    public function deleteProductDB($id){
        $product = Product::find($id);
        $product->delete();
        return redirect('/home');
    }

    public function addProduct(){
        
        $user = auth()->user();
        $type = Type::all();

        if ($user->role == 'Admin')
            {
                return view('addproduct', ['type' => $type]);
            }
    }

    public function addProductDB(Request $request){
        
        $user = auth()->user();
        $type = Type::all();

        $this->validate($request,[
            'filename' => ['required', 'string'],
    		'name' => ['required', 'string', 'min:5', 'unique:products'],
            'description' => ['required', 'string', 'min:10'],
            'type_id' => ['required', 'integer'],
            'stock' => ['required', 'integer', 'min:1'],
            'price' => ['required', 'integer', 'min:5000'],
        ]);

        Product::create([
            'image' => $request->filename,
            'name' => $request->name,
            'description' => $request->description,
            'type_id' => $request->type_id,
            'stock' => $request->stock,
            'price' => $request->price
        ]);
        
    }

    public function addType(){
        $type = Type::all();
        $user = auth()->user();

        if ($user->role == 'Admin')
            {
                return view('addstationarytype', ['type' => $type]);
            }
    }

    public function addTypeDB(Request $request){

        $this->validate($request,[
    		'name' => ['required', 'string', 'unique:types'],
    	]);
 
        Type::create([
            'name' => $request->name,
        ]);
        
 
    	return redirect('/addType')->with(['success', 'Stationary types has been added successfully.']);
    }

    public function editType(){
        $type = Type::all();
        $user = auth()->user();

        if ($user->role == 'Admin')
            {
                return view('updateproducttype', ['type' => $type]);
            }
    }

    public function updateType(Request $request){

        $this->validate($request,[
        'name' => ['required', 'string'],
        ]);
        
        $updateType = Type::find($request->id);
        $updateType->name = $request->name;
        $updateType->save();
        return redirect('/editType');
    }

    public function deleteType($id){
        $type = Type::find($id);
        $type->delete();
        return redirect('/editType');
    }

    public function search(Request $request){
        
		$search = $request->search;

        $product = Product::where('name','LIKE','%'.$search.'%')->paginate(4);
        
        $user = auth()->user();

        if (Auth::guest())
        {
            return view ('guesthome', ['product' => $product]);
        }

        if ($user->role == 'Admin')
            {
                return view('adminhome', ['product' => $product]);
            }
        else if ($user->role == 'Member')
        {
            return view('memberhome', ['product' => $product]);
        }
		
 
    }
}
