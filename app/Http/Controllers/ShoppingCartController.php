<?php

namespace App\Http\Controllers;

use App\Product;
use App\ShoppingCart;
use App\Transaction;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ShoppingCartController extends Controller
{
    public function index(){
        $shoppingcart = ShoppingCart::all();
        $user = auth()->user();
        return view('shoppingcart', ['shoppingcart' => $shoppingcart, 'user' => $user]);
    }

    public function add(Request $request){

        ShoppingCart::updateOrCreate(
            [
            'product_id'=>$request->product_id,
            'user_id'=>Auth::user()->id
            ],
            [
            'quantity'=>$request->quantity
            ]);
        return redirect('/home');
            
    }

    public function edit($id){
        $shoppingcart = ShoppingCart::find($id);
        return view('updateshoppingcart', ['shoppingcart' => $shoppingcart]);
    }

    public function editDB($id, Request $request){
        $this->validate($request,[
        'quantity' => 'required',
        ]);
    
        $update = ShoppingCart::find($id);
        $update->quantity = $request->quantity;
        $update->save();

        return redirect('/shoppingcart');
    }

    public function delete($id){
        $shoppingcart = ShoppingCart::find($id);
        $shoppingcart->delete();
        return redirect('/shoppingcart');
    }

    public function checkout(){
        $user = auth()->user();
        $shoppingcart = ShoppingCart::where('user_id', 'like', $user->id)->get();
        
        foreach ($shoppingcart as $sc){
            Transaction::create([
                'user_id' => $user->id ,
                'shoppingcart_id' => $user->cart_token,
                'product_id' => $sc->product->id,
                'quantity' => $sc->quantity
                ]);

            $product = Product::where('id', 'like', $sc->product->id)->first();
            $product->stock = $product->stock - $sc->quantity;
            $product->save();

            $sc->delete();
            
        }

            $update = User::find($user->id);
            $update->cart_token += 1;
            $update->save();
        
        return redirect('/home');
    }
}
