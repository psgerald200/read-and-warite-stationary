<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $user = auth()->user();

        if (Auth::guest())
        {
            return view ('guesthome');
        }
        else
        {
            if ($user->role == 'Admin')
            {
                return view ('adminhome');
            }
            else if ($user->role == 'Member')
            {
                return view ('memberhome');
            }
        }
        
        
    }
}
