<?php

namespace App\Http\Controllers;

use App\Product;
use App\Transaction;
use Illuminate\Http\Request;

class TransactionController extends Controller
{
    public function index(){
        $user = auth()->user();
        $transaction = Transaction::where('user_id', 'like', $user->id)->get();
        

        return view('transactionhistory', ['transaction' => $transaction]);
    }

    
}
