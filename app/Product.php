<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    //

    public function Type(){
        return $this->belongsTo(Type::class);
    }

    public function ShoppingCart(){
        return $this->hasMany(ShoppingCart::class);
    }

    public $table='products';

    
    protected $fillable = ['image', 'name', 'description', 'type_id', 'stock', 'price'];
}
